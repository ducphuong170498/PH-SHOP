<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\CategoryProduct;
use App\Models\Comment;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function getHome() {
        $data['featured'] = Product::where('prod_featured',1)->orderBy('id','DESC')->get();
      //  $data['news'] = Product::orderBy('id', 'DESC')->get();
       $data['categories'] = CategoryProduct::orderBy('id', 'DESC')->get();
  //      $data['categories'] = CategoryProduct::all();
        return view('test',$data);
    }
    public function getDetail (Request $request, $id) {
        $data['item'] = Product::find($id);
//        $item = Product::find($id);
        $data['comments'] = Comment::where('product_id', $id)->get();
//        return view('details')->with(compact('data'));
        return view('details',$data);
    }

    public function getCategoryProduct (Request $request, $id) {
        $data['cateName'] = CategoryProduct::find($id);
        $data['items'] = Product::where('category_id', $id)->orderBy('id','DESC')->paginate(1);
        return view('frontend.category',$data);
    }
    public function postComment (Request $request,$id) {
        $comment = new Comment;
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->product_id = $id;
        $comment->name = $request->name;
        $comment->content = $request->content;
        $comment->save();
        return back();
    }
    public function getSearch (Request $request) {
        $result = $request->result;
        $data['keyword'] = $result;
        $result = str_replace(' ' , '%', $result);
        $data['items'] = Product::query()->where('name', 'like', '%'.$result.'%')->get();
        return view('frontend.search',$data);
    }

}
