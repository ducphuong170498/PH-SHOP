<?php
namespace App\Http\Controllers;
use App\Models\Product;
use Cart;
use Mail;
use Illuminate\Http\Request;


class CartController extends Controller
{
    public function getAddCart($id) {
        $product = Product::find($id);
        Cart::add(['id' => $id, 'name' => $product->name , 'qty' => 1, 'price' => $product->price, 'weight' => 550, 'options' => ['img' => $product->hinhanh]]);

      return redirect('cart/show');
    }

    public function getShowCart() {
        $data['total'] = Cart::total();
        $data['items'] = Cart::content();
        return view('frontend.cart',$data);
    }

    public function getDeleteCart($id) {
        if($id == 'all') {
            Cart::destroy();
        } else {
            Cart::remove($id);
        }

        return back();
    }

    public function getUpdateCart(Request $request) {
        Cart::update($request->rowId, $request->qty);
    }

    public function postComplete(Request $request)
    {
        $data['info'] = $request->all();
        $data['total'] = Cart::total();
        $data['cart'] = Cart::content();
        $email = $request->email;

        Mail::send(
            'frontend.email',
            $data,
            function ($message) use ($email)
            {
                $message->from('ducphuong170498@gmail.com', 'phuongvd1');
                $message->to($email, $email);
                $message->cc('ducphuong170498@gmail.com', 'phuongvd3');
                $message->subject('Xac nhan hoa don thanh cong');
            });
        dd(123);
        return redirect('complete');
    }

    public function getComplete()
    {
        return view('frontend.complete');
    }
}
