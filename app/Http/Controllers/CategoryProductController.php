<?php

namespace App\Http\Controllers;

use App\Models\CategoryProduct;
use Illuminate\Http\Request;

class   CategoryProductController extends Controller
{

    public function index()
    {
        $category = CategoryProduct::orderBy('id','DESC')->get();
        return view('admin.categoryproduct.index')->with(compact('category'));
    }


    public function create()
    {
        return view('admin.categoryproduct.create');
    }


    public function store(Request $request)
    {
        CategoryProduct::create($request->all());
      //  return redirect()->route('categoryproduct.index')->with('status', 'Thêm danh mục thành công');
        return redirect()->back()->with('status', 'Thêm danh mục thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = CategoryProduct::find($id);
        return view('admin.categoryproduct.edit')->with(compact('category'));
    }


    public function update(Request $request, $id)
    {
        $category = CategoryProduct::find($id);
        $category->update($request->all());
        return redirect()->route('category-product.index')->with('status','Cập nhật thành công');
    }


    public function destroy(CategoryProduct $categoryProduct)
    {
        $categoryProduct->delete();
        return redirect()->route('category-product.index')->with('status','Xóa thành công');
    }
}
