<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\CategoryProduct;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $product = Product::orderBy('id','DESC')->with('categoryProduct')->get();
        return view('admin.product.index')->with(compact('product'));
    }


    public function create()
    {
        $category = CategoryProduct::with('product')->orderBy('id','DESC')->get();
        return view('admin.product.create')->with(compact('category'));
    }


    public function store(Request $request)
    {
       // $data = $request->all();
        $data = $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
                'hinhanh' => 'required|image|mimes: jpg,png,jpeg,gif,svg',
//                |max:10240|dimensions:min_width=100,
//                min_height=100,max_width=5000,max_height=5000',
                'price' => 'required',
                'category_id' => 'required',
                'quantity' => 'required',
                'color' => 'required',
                'stock_date' => 'required',
            ],
            [
                'name.unique' => 'Tên sản phẩm đã có, xin điền tên khác',
                'description.required' => 'Tên mô tả đã có, xin điền tên khác',
                'name.required' => 'Tên sản phẩm phải có nhé',
                'price.required' => 'Giá tiền phải có nhé',
                'color.required' => 'Màu sắc phải có nhé',
                'quantity.required' => 'Số lương phải có',
                'stock_date.required'=> 'Ngày nhập phải có'
            ]
        );
        $product = new Product();
        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->category_id = $data['category_id'];
        $product->quantity = $data['quantity'];
        $product->color = $data['color'];
        $product->stock_date = $data['stock_date'];
        $get_image = $request->hinhanh;
//        dd($get_image);
        $path = 'uploads/product/';
        $get_name_image = $get_image->getClientOriginalName();
        $name_image = current(explode('.',$get_name_image));
        $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
        $get_image->move($path,$new_image);
        $product->hinhanh = $new_image;
        $product->save();


//     Product::create($request->all());

      return redirect()->route('product.index')->with('status', 'Thêm sản phẩm thành công');
  //      return redirect()->back()->with('status', 'Thêm sản phẩm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = CategoryProduct::query()->orderBy('id', 'DESC')->get();
        return view('admin.product.edit')->with(compact('product','category'));
    }


    public function update(Request $request, $id)
    {
        $data = $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
                'hinhanh' => 'required|image|mimes: jpg,png,jpeg,gif,svg',
//                |max:10240|dimensions:min_width=100,
//                min_height=100,max_width=5000,max_height=5000',
                'price' => 'required',
                'category_id' => 'required',
                'quantity' => 'required',
                'color' => 'required',
                'stock_date' => 'required',
            ],
            [
                'name.unique' => 'Tên sản phẩm đã có, xin điền tên khác',
                'description.required' => 'Tên mô tả đã có, xin điền tên khác',
                'name.required' => 'Tên sản phẩm phải có nhé',
                'price.required' => 'Giá tiền phải có nhé',
                'color.required' => 'Màu sắc phải có nhé',
                'quantity.required' => 'Số lương phải có',
                'stock_date.required'=> 'Ngày nhập phải có'
            ]
        );
        $product = Product::find($id);
        $product->name = $data['name'];
        $product->description  = $data['description'];
        $product->price = $data['price'];
        $product->category_id = $data['category_id'];
        $product->quantity = $data['quantity'];
        $product->color = $data['color'];
        $product->stock_date = $data['stock_date'];
        $get_image = $request->hinhanh;

        if ($get_image) {
            $path = 'uploads/product/'.$product->hinhanh;

            if (file_exists($path)) {
                unlink($path);
            }

            $path = 'uploads/product/';
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move($path,$new_image);
            $product->hinhanh = $new_image;


        }
        $product->save();

        return redirect()->route('product.index')->with('status','Cập nhật thành công');
    }


    public function destroy(Product $product)
    {
//        Product::find($id)->delete();

        $path = 'uploads/product'.$product->id;

        if(file_exists($path)) {
            unlink($path);
        }

        $product->delete();
        return redirect()->route('product.index')->with('status','Xóa thành công');
    }
}
