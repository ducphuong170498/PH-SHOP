<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\CategoryProduct;
class Product extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    public $table = 'products';
    protected $fillable = [
        'name',
        'description',
        'price',
        'category_id',
        'quantity',
        'color',
        'stock_date',
        'hinhanh'
    ];
    public function categoryProduct()
    {
        return $this->belongsTo(CategoryProduct::class, 'category_id', 'id');
    }

}
