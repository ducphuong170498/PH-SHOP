<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Product;
class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';
    protected $fillable = [
        'email',
        'name',
        'product_id',
        'content',
    ];
    public function product() : BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

}
