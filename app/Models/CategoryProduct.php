<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CategoryProduct extends Model
{
    use HasFactory;
    protected $table = 'category_product';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
    ];

    public function product() : HasMany
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}
