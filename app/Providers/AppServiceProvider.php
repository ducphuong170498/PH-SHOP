<?php

namespace App\Providers;

use App\Models\CategoryProduct;
use Illuminate\Support\ServiceProvider;
use Illuminate \ Pagination \ Paginator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['categories'] = CategoryProduct::all();
        view()->share($data);
        Paginator::useBootstrap();
    }
}
