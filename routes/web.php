<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryProductController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/home123', function () {
//    return view('frontend.home');
//});
//Route::get('/clone', 'FrontendController@getHome');
//Route::get('/clone', [
//    'uses' => 'FrontendController@getHome'
//]);

Route::get('/clone', [FrontendController::class,'getHome']);
Route::get('detail/{id}/', [FrontendController::class,'getDetail']);
Route::post('detail/{id}/', [FrontendController::class,'postComment']);


Route::get('detail/{id}/', [FrontendController::class,'getDetail']);

Route::get('search', [FrontendController::class,'getSearch']);

Route::group(['prefix' => 'cart'], function() {
    Route::get('add/{id}', [CartController::class, 'getAddCart']);
    Route::get('show', [CartController::class, 'getShowCart']);
    Route::get('delete/{id}', [CartController::class, 'getDeleteCart']);
    Route::get('update', [CartController::class, 'getUpdateCart']);
    Route::post('show', [CartController::class, 'postComplete']);
});

Route::get('complete',[CartController::class, 'getComplete']);

//Route::get('cart/add/{id}', [CartController::class,'getAddCart']);

Route::get('category-product/{id}/', [FrontendController::class,'getCategoryProduct']);

Route::get('/', [IndexController::class,'home']);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/test', function () {
   return view('layouts.master');
});
Route::get('/thu', function () {
   return view('test');
});
Auth::routes();

Route::resource('admin/product', ProductController::class);
Route::resource('admin/category-product', CategoryProductController::class);
Route::resource('admin/post', PostController::class);



