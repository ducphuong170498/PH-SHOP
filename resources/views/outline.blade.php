<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container-fluid">
    <div class="row header">
        <div class="col-12 col-sm-3 col-md-3">
{{--            <img th:src="@{/img/1024.png}" class="logo" />--}}
            <i class="fa-sharp fa-solid fa-hippo"></i>
            <i class="fa-sharp fa-solid fa-house"></i>
        </div>
        <div class="col-12 col-sm-9 col-md-9">
            <h1>CMS-P-SHOP</h1>
        </div>
    </div>
    <div class="row body">
        <div class="col-12 col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{route('category-product.index')}}">Quản lý danh mục</a></li>
                <li class="list-group-item"><a href="{{route('product.index')}}">Quản lý product</a></li>
                <li class="list-group-item"><a href="{{route('post.index')}}">Quản lý tin tức</a></li>
{{--                <li class="list-group-item"><a href="{{route('user.index')}}">Quản lý user</a></li>--}}
                <li class="list-group-item"><a>Thoát</a></li>
            </ul>

        </div>

        <div class="col-12 col-sm-9">
            @yield('content')
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/js/bootstrap.min.js" integrity="sha512-1/RvZTcCDEUjY/CypiMz+iqqtaoQfAITmNSJY17Myp4Ms5mdxPS5UV7iOfdZoxcGhzFbOm6sntTKJppjvuhg4g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>
</html>
