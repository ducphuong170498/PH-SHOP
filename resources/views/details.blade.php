@extends('master')
@section('title', 'Trang chủ')
@section('main')
    <link rel="stylesheet" href="{{asset('css/details.css')}}">
    <div id="wrap-inner">
        <div id="product-info">
            <div class="clearfix"></div>
            <h3>{{$item->name}}</h3>
            <div class="row">
                <div id="product-img" class="col-xs-12 col-sm-12 col-md-6 text-center">
                    <img style="height: 336px" src="{{asset('uploads/product/'.$item->hinhanh)}}">
                </div>
                <div id="product-details" class="col-xs-12 col-sm-12 col-md-6">
                    <p>Giá: <span class="price">{{number_format($item->price,0,',','.')}}</span></p>
                    <p>Bảo hành: 1 đổi 1 trong 12 tháng</p>
                    <p>Phụ kiện: Dây cáp sạc, tai nghe</p>
                    <p>Tình trạng: Máy mới 100%</p>
                    <p>Khuyến mại: Hỗ trợ trả góp 0% dành cho các chủ thẻ Ngân hàng Sacombank</p>
                    <p>Còn hàng: Còn hàng</p>
                    <p class="add-cart text-center"><a href="{{asset('cart/add/'.$item->id)}}">Đặt hàng online</a></p>
                </div>
            </div>
        </div>
        <div id="product-detail">
            <h3>Chi tiết sản phẩm</h3>
            <p class="text-justify">{{$item->description}}</p>
        </div>
        <div id="comment">
            <h3>Bình luận</h3>
            <div class="col-md-9 comment">
                <form method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input required type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="name">Tên:</label>
                        <input required type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="cm">Bình luận:</label>
                        <textarea required rows="10" id="cm" class="form-control" name="content"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-default">Gửi</button>
                    </div>
                </form>
            </div>
        </div>
        <div id="comment-list">
            <ul>
                @foreach($comments as $com)
                <li class="com-title">
                    {{$com->name}}
                    <br>
{{--                    <span>{{$com->created_at}}</span>--}}
                    <span>{{date('d/m/Y H:i', strtotime($com->created_at))}}</span>
                </li>
                <li class="com-details">
                    {{$com->content}}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
