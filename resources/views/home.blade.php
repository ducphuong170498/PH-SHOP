@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in! oke???') }}


                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<! DOCTYPE html>
<html lang="en">
<head>
    <title> Hoang Ha Mobile </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"> </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"> </script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<style>
    html {
        position: relative;
        min-height: 100%;
    }
    body {
        padding-top: 4.5rem;
        margin-bottom: 4.5rem;
    }
    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 3.5rem;
        line-height: 3.5rem;
        background-color: #ccc;
    }
    .bg-dark {
        background-color: #6a9aca!important;
    }
    .nav-link:hover {
        transition: all 0.4s;
    }
    .nav-link-collapse:after {
        float: right;
        content: '\f067';
        font-family: 'FontAwesome';
    }
    .nav-link-show:after {
        float: right;
        content: '\f068';
        font-family: 'FontAwesome';
    }
    .nav-item ul.nav-second-level {
        padding-left: 0;
    }
    .nav-item ul.nav-second-level > .nav-item {
        padding-left: 20px;
    }
    @media (min-width: 992px) {
        .sidenav {
            position: absolute;
            top: 0;
            left: 0;
            width: 230px;
            height: calc(100vh - 3.5rem);
            margin-top: 3.5rem;
            background: #343a40;
            box-sizing: border-box;
            border-top: 1px solid rgba(0, 0, 0, 0.3);
        }
        .navbar-expand-lg .sidenav {
            flex-direction: column;
        }
        .content-wrapper {
            margin-left: 230px;
        }
        .footer {
            width: calc(100% - 230px);
            margin-left: 230px;
        }
    }
</style>
<body>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
    <a href="{{route('product.index')}}" class="btn btn-primary float-end">Danh sách sản phẩm</a>
    <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarCollapse"
        aria-controls="navbarCollapse"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"> </span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto sidenav" id="navAccordion">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('product.index')}}"> Home  </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> About </a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link nav-link-collapse"
                    href="#"
                    id="hasSubItems"
                    data-toggle="collapse"
                    data-target="#collapseSubItems2"
                    aria-controls="collapseSubItems2"
                    aria-expanded="false"> Quản lý tin tức </a>
                <ul class="nav-second-level collapse" id="collapseSubItems2" data-parent="#navAccordion">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link nav-link-collapse"
                    href="{{route('product.index')}}"
                    id="hasSubItems"
                    data-toggle="collapse"
                    data-target="#collapseSubItems2"
                    aria-controls="collapseSubItems2"
                    aria-expanded="false"> Quản lý danh mục </a>
                <ul class="nav-second-level collapse" id="collapseSubItems2" data-parent="#navAccordion">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link nav-link-collapse"
                    href="{{route('product.index')}}"
                    id="hasSubItems"
                    data-toggle="collapse"
                    data-target="#collapseSubItems2"
                    aria-controls="collapseSubItems2"
                    aria-expanded="false"> Quản lý sản phẩm </a>
                <ul class="nav-second-level collapse" id="collapseSubItems2" data-parent="#navAccordion">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 2.2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"> Gallery </a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link nav-link-collapse"
                    href="#"
                    id="hasSubItems"
                    data-toggle="collapse"
                    data-target="#collapseSubItems4"
                    aria-controls="collapseSubItems4"
                    aria-expanded="false"> Contact us </a>
                <ul class="nav-second-level collapse" id="collapseSubItems4" data-parent="#navAccordion">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 4.1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 4.2</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="nav-link-text">Item 4.2</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
        <form class="form-inline ml-auto mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn  my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<main class="content-wrapper">
    <div class="container-fluid">
        <h1> CMS - P-SHOP </h1>
    </div>
</main>
<footer class="footer">
    <div class="container">
        <div class="text-center">
            <h1> Phần mềm quản lý CMS </h1>
        </div>
    </div>
</footer>
<script>
    $(document).ready(function() {
        $('.nav-link-collapse').on('click', function() {
            $('.nav-link-collapse').not(this).removeClass('nav-link-show');
            $(this).toggleClass('nav-link-show');
        });
    });
</script>
</body>
</html>
