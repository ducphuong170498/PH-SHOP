@extends('outline')


@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Thêm danh mục sản phẩm</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('category-product.index')}}" class="btn btn-primary float-end">Danh sách danh mục sản phẩm</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="{{route('category-product.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Tên danh mục sản phẩm</strong>
                                <input type="text" name="name" class="form-control" placeholder="Nhập tên danh mục sản phẩm">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Lưu</button>
                </form>

            </div>
        </div>
    </div>
@endsection

