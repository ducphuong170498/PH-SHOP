@extends('outline')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Quản lý danh mục sản phẩm</h4>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('category-product.create')}}" class="btn btn-primary float-end">Thêm mới</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ Session::get('status') }}
                    </div>
                @endif
                <table class = "table table-bordered">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên danh mục</th>
                            <th>Tùy chọn</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($category as $key => $item)
                            <tr>
                                 <td>{{++$key}}</td>
                                 <td>{{$item->name}}</td>
                                <td>
                                    <a href="{{route('category-product.edit',[$item->id])}}" class="btn btn-primary">Edit</a>
                                    <form action = "{{route('category-product.destroy',[$item->id])}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button onclick="return confirm(' Bạn có chắc chắn muốn xóa chapter này không? ')" class = "btn btn-danger">Delete </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
