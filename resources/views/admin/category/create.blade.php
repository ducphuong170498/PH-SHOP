@extends('outline')


@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Thêm sản phẩm</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('categoryproduct.index')}}" class="btn btn-primary float-end">Danh sách danh mục sản phẩm</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="{{route('categoryproduct.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Tên sản phẩm</strong>
                                <input type="text" name="name" class="form-control" placeholder="Nhập tên sản phẩm">
                            </div>
                            <div class="form-group">
                                <strong>Mô tả</strong>
                                <input type="text" name="description" class="form-control" placeholder="Nhập mô tả sản phẩm">
                            </div>
                            <div class="form-group">
                                <strong>Giá</strong>
                                <input type="text" name="price" class="form-control" placeholder="Nhập giá">
                            </div>
                            <div class="form-group">
                                <strong>Số lượng</strong>
                                <input type="number" name="quantity" class="form-control" placeholder="Nhập giá">
                            </div>
                            <div class="form-group">
                                <strong>Màu</strong>
                                <input type="text" name="color" class="form-control" placeholder="Nhập màu sắc">
                            </div>
                            <div class="form-group">
                                <strong>Ngày nhập kho</strong>
                                <input type="date" name="ngay_nhap" class="form-control">
                            </div>

                            <div class="form-group">
                                <label >Thuộc danh mục</label>
                                <select name="category_id" class="custom-select">
                                    @foreach($category as $key => $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Lưu</button>
                </form>

            </div>
        </div>
    </div>
@endsection

