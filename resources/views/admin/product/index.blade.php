@extends('outline')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Quản lý sản phẩm </h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('product.create')}}" class="btn btn-primary float-end">Thêm mới</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ Session::get('status') }}
                    </div>
                @endif
                <table class = "table table-bordered">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên sản phẩm</th>
                            <th>Mô tả</th>
{{--                            <th>Giá</th>--}}
{{--                            <th>Màu sắc</th>--}}
                            <th>Thuộc danh mục</th>
                            <th>Tùy chọn</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($product as $key => $item)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->description}}</td>
                                <td><img src="{{asset('uploads/product/'.$item->hinhanh)}}" height="250" width="180"></td>
{{--                                <td>{{$item->price}}</td>--}}
{{--                                <td>{{$item->color}}</td>--}}
                                <td>{{$item->categoryProduct->name}}</td>
                                <td>
                                    <a href="{{route('product.edit',[$item->id])}}" class="btn btn-primary">Edit</a>
                                    <form action = "{{route('product.destroy',[$item->id])}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button onclick="return confirm(' Bạn có chắc chắn muốn xóa chapter này không? ')" class = "btn btn-danger">Delete </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
