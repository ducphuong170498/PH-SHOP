@extends('outline')


@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Sửa sản phẩm</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('product.index')}}" class="btn btn-primary float-end">Danh sách sản phẩm</a>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class = "alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <form action="{{route('product.update',[$product->id])}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Tên sản phẩm</strong>
                                <input type="text" name="name" value="{{$product->name}}" class="form-control" placeholder="Nhập tên sản phẩm">
                            </div>
                            <div class="form-group">
                                <strong>Mô tả</strong>
                                <input type="text" name="description" value="{{$product->description}}" class="form-control" placeholder="Nhập mô tả sản phẩm">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hình ảnh sản phẩm</label>
                                <input type="file" class="form-control-file" name="hinhanh">
                                <td><img src="{{asset('uploads/product/'.$product->hinhanh)}}" height="250" width="180"></td>
                            </div>
                            <div class="form-group">
                                <strong>Giá</strong>
                                <input type="text" name="price" value="{{$product->price}}" class="form-control" placeholder="Nhập giá">
                            </div>
                            <div class="form-group">
                                <strong>Số lượng</strong>
                                <input type="number" name="quantity" value="{{$product->quantity}}" class="form-control" placeholder="Nhập giá">
                            </div>
                            <div class="form-group">
                                <strong>Màu</strong>
                                <input type="text" name="color" value="{{$product->color}}" class="form-control" placeholder="Nhập màu sắc">
                            </div>
                            <div class="form-group">
                                <strong>Ngày nhập kho</strong>
                                <input type="date" name="stock_date" value="{{$product->stock_date}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label >Thuộc danh mục</label>
                                <select name="category_id" class="custom-select">
                                    @foreach($category as $key => $value)
                                        <option {{$value->id == $product->category_id ? 'selected' : ''}}  value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Update</button>
                </form>

            </div>
        </div>
    </div>
@endsection
