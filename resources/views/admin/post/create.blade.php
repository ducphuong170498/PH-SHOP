@extends('outline')


@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Thêm sản phẩm</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('post.index')}}" class="btn btn-primary float-end">Danh sách danh mục sản phẩm</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="{{route('post.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Tên bài viết</strong>
                                <input type="text" name="name" class="form-control" placeholder="Nhập tên bài viết">
                            </div>
                            <div class="form-group">
                                <strong>Tiêu đề</strong>
                                <input type="text" name="title" class="form-control" placeholder=" tiêu đề ">
                            </div>
                            <div class="form-group">
                                <strong>Content</strong>
                                <input type="text" name="content" class="form-control" placeholder=" Nhập nôi dung">
                            </div>


{{--                            <div class="form-group">--}}
{{--                                <label >Thuộc danh mục</label>--}}
{{--                                <select name="category_id" class="custom-select">--}}
{{--                                    @foreach($category as $key => $value)--}}
{{--                                        <option value="{{$value->id}}">{{$value->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Lưu</button>
                </form>

            </div>
        </div>
    </div>
@endsection

