@extends('master')
@section('title', 'Danh mục sản phẩm')
@section('main')
    <link rel="stylesheet" href="{{asset('css/category.css')}}">
    <div id="wrap-inner">
        <div class="products">
            <h3>{{$cateName->name}}</h3>
            <div class="product-list row">
                @foreach($items as $item)
                <div class="product-item col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img src="{{asset('uploads/product/'.$item->hinhanh)}}" class="img-thumbnail"></a>
                    <p><a href="#">{{$item->name}}</a></p>
                    <p class="price">{{number_format($item->price,0,',','.')}}</p>
                    <div class="marsk">
                        <a href="#">Xem chi tiết</a>
                    </div>
                </div>
                @endforeach

            </div>
        </div>

        <div id="pagination">
           <ul class="pagination pagination-lg justify-content-center">
{{--                <li class="page-item">--}}
{{--                    <a class="page-link" href="#" aria-label="Previous">--}}
{{--                        <span aria-hidden="true">&laquo;</span>--}}
{{--                        <span class="sr-only">Previous</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="page-item disabled"><a class="page-link" href="#">1</a></li>--}}
{{--                <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
{{--                <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                <li class="page-item">--}}
{{--                    <a class="page-link" href="#" aria-label="Next">--}}
{{--                        <span aria-hidden="true">&raquo;</span>--}}
{{--                        <span class="sr-only">Next</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
               {{ $items->links() }}
            </ul>

        </div>
{{--        <div class="row text-center">--}}
{{--            <div class="col-md-12">--}}
{{--                <ul class="pagination pagination-lg">--}}
{{--                    <li><a href="#">&laquo;</a></li>--}}
{{--                    <li class = "active disable"><span>1</span></li>--}}
{{--                    <li><a href="#">2</a></li>--}}
{{--                    <li><a href="#">3</a></li>--}}
{{--                    <li><a href="#">4</a></li>--}}
{{--                    <li><a href="#">5</a></li>--}}
{{--                    <li><a href="#">&laquo;</a></li>--}}
{{--                </ul>--}}
{{--                {{$items->links()}}--}}
{{--            </div>--}}

{{--        </div>--}}
    </div>
@endsection
